# Install/Start

## Node API

- POST: '/' - takes a msg and option and adds it to the queue. the msg must be an object and the options are any valid option for bull's que.add method
- POST: '/shutdown' - takes optional params: time (in miliseconds) or now (true/false)
- GET: '/pause' - pauses the queue
- GET: '/resume' - resumes the queue
- DELETE: '/' - empties the queue

## Notes

Although there is a delete route, the queue will automatically clear all jobs older than 5 sec every 30sec.

## Requirements

- Redis: it will not work with a cluster

## Options

- update the .env

```
PORT=3000
NODE_ENV=development
REDIS_HOSTNAME=localhost
REDIS_PORTS=6379
APP_NAME='ws-back-end-2020'
CONCURRENCY=100
CHANNEL='Queue'
```
