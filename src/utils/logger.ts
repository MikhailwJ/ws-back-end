import * as _ from 'lodash';
import { createLogger, format, transports } from 'winston';
_.mixin({ pascalCase: _.flow(_.camelCase, _.upperFirst) });

const { APP_NAME = 'Server' } = process.env;
const pascalCase = (_ as any).pascalCase;
const { combine, timestamp, align, colorize, splat, printf } = format;
const stringify = format(info => (typeof info.message === 'object' ? { ...info, message: '\n' + JSON.stringify(info.message) } : info));

function printArgs(meta) {
  return meta ? '\n' + JSON.stringify(meta, null, 2) : '';
}

const Logger = (id?: string) =>
  createLogger({
    format: combine(
      stringify(),
      align(),
      splat(),
      timestamp({ format: 'YYYY-MM-DD HH:mm:ss' }),
      printf(info => {
        const { level, message, ...args } = info;
        return colorize().colorize(
          level,
          `[${APP_NAME || 'APP'}] ${process.pid} - [${pascalCase(level)}]${id ? `[${id}]` : ''}:${message} ${printArgs(args.meta)}`,
        );
      }),
    ),
    transports: [new transports.Console({ level: 'debug' })],
  });

if (process.env.NODE_ENV !== 'production') {
  Logger('winston').debug('Logging initialized at debug level');
}

export { Logger };
