// tslint:disable-next-line: no-var-requires
require('dotenv').config();
import { normalizePort } from './helpers';
import { Logger } from './utils/logger';
import { MessageQueue } from './msg-queue';

const { PORT = 3000, NODE_ENV = 'development', APP_NAME = 'Server', CHANNEL, CONCURRENCY: concurrency } = process.env;
const que = new MessageQueue(CHANNEL, { concurrency });

// for demo only. normaly this would be on top
import { app } from './app';

que.getDefault().add({ test: 1234 });

/**
 * Get port from environment and store in Express.
 */

const port = normalizePort(PORT);
app.set('port', port);

/**
 * Create HTTP server.
 */

const server = app.listen(port);

/**
 * Listen on provided port, on all network interfaces.
 */
server.on('error', onError);
server.on('listening', onListening);

/**
 * Event listener for HTTP server "error" event.
 */

function onError(error) {
  Logger().error('Something went wrong. 😶');
  if (error.syscall !== 'listen') {
    throw error;
  }

  const bind = typeof port === 'string' ? 'Pipe ' + port : 'Port ' + port;

  // handle specific listen errors with friendly messages
  switch (error.code) {
    case 'EACCES':
      console.error(bind + ' requires elevated privileges');
      process.exit(1);
      break;
    case 'EADDRINUSE':
      console.error(bind + ' is already in use');
      process.exit(1);
      break;
    default:
      throw error;
  }
}

/**
 * Event listener for HTTP server "listening" event.
 */

function onListening() {
  const log = Logger();
  const addr = server.address();
  const bind = typeof addr === 'string' ? 'pipe ' + addr : 'port ' + addr.port;
  log.info('Listening on ' + bind);
  log.info('All is well.');
  log.info(`Running ${APP_NAME} on http://localhost:${PORT} in ${NODE_ENV} mode. 🚀`);
  log.warn('Press CTRL-C to stop');
}
