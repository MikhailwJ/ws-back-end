import { Router } from 'express';
import * as uuid from 'uuid/v1';
import { MessageQueue } from '../msg-queue';
import { Logger } from '../utils/logger';

const router = Router();
const log = Logger();
const MQ = MessageQueue.default;

router.post('/', (req, res) => {
  try {
    const { msg, options } = req.body;
    if (!msg) return res.status(403).json({ error: 'msg value is not set' });
    if (typeof msg !== 'object') return res.status(403).json({ error: 'msg value must be an object' });
    const id = uuid();
    Logger(id).info('Adding new job to the queue');
    Logger(id).info(msg);
    MQ.add({ ...msg, id }, options);
    res.sendStatus(204);
  } catch (error) {
    res.status(403).json({ error: error.message });
  }
});

router.get('/pause', async (req, res) => {
  log.warn('Pausing queue...');
  await MQ.pause();
  res.sendStatus(204);
});

router.get('/resume', async (req, res) => {
  log.warn('Queue resumming...');
  await MQ.resume();
  res.sendStatus(204);
});

router.post('/shutdown', (req, res) => {
  const { time = 10000, now } = req.body;
  if (isNaN(Number(time))) {
    return res.status(403).json({ error: 'invalid time set' });
  }

  if (!now && time > 0) {
    log.warn(`Server will shutdown in ${time} seconds`);
    return setTimeout(
      () =>
        MQ.close().then(() => {
          log.info('Shutting down...');
          process.exit();
        }),
      +time,
    );
  }

  MQ.close().then(() => {
    log.info('Shutting down...');
    process.exit();
  });
});

router.delete('/', (req, res) => {
  log.warn('Queue is being emptied...');
  MQ.empty().then(() => res.sendStatus(204));
});

export { router as indexRouter };
