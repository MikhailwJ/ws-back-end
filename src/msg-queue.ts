import * as Bull from 'bull';
import { REDIS_CLIENT_OPTS } from './constants';
import { Logger } from './utils/logger';
import * as redis from 'redis';

const logger = Logger();

interface QueueOptions {
  autoPurge?: boolean;
  concurrency?: string | number;
  clearInterval?: number;
  clearGracePeriod?: number;
}

abstract class Queue {
  private static queues: Array<{ channel: string; queue: Bull.Queue }> = [];
  static default: Bull.Queue;
  static add: any;
  static pause: any;
  static resume: any;
  static close: any;
  static empty: any;
  autoPurge = true;
  clearInterval = 5000;
  clearGracePeriod = 5000;

  constructor(channel: string, { concurrency, clearInterval, clearGracePeriod, autoPurge }: QueueOptions) {
    const client = new redis.RedisClient(REDIS_CLIENT_OPTS);
    const queOptions: Bull.QueueOptions = { redis: REDIS_CLIENT_OPTS };
    const existing = Queue.queues.find(this.findByChannel(channel));

    if (existing) {
      logger.info(`Queue already exisits: ${channel} (channel)`);
      this.setDefault(channel);
      return;
    }

    if (!channel) {
      logger.error('No Channel Set.');
      throw new Error('No Channel Set');
    }
    const queue = new Bull(channel, queOptions);

    if (!concurrency) {
      logger.warn('no CONCURRENCY env variable set. defaulting to 1...');
    }

    if (concurrency && isNaN(Number(concurrency))) {
      logger.error('Invalid CONCURRENCY variable set');
      throw new Error('Invalid CONCURRENCY');
    }

    queue.process(+concurrency || 1, this.process);

    // Auto clean all jobs that completed over 5 seconds ago from the queue every 30sec
    if (clearInterval && !isNaN(Number(clearInterval))) this.clearInterval = clearInterval;
    if (clearGracePeriod && !isNaN(Number(clearGracePeriod))) this.clearGracePeriod = clearGracePeriod;
    if (autoPurge === false) this.autoPurge = autoPurge;

    if (this.autoPurge) {
      setInterval(() => {
        queue.getCompletedCount().then(qty => {
          if (qty > 0) {
            queue.clean(this.clearInterval, 'completed');
          }
        });
      }, this.clearGracePeriod);
    }

    queue.on('completed', this.onCompleted);

    queue.on('cleaned', this.onCleaned);

    queue.on('error', this.onQueueError);

    client.on('error', this.onRedisError);

    Queue.queues.push({ channel, queue });
    Queue.default = queue;

    logger.info(`New queue started on channel: ${channel}`);
  }

  getDefault() {
    return Queue.default;
  }

  getQueue(channel: string) {
    return Queue.queues.find(this.findByChannel(channel));
  }

  setDefault(channel: string) {
    const que = Queue.queues.find(this.findByChannel(channel));
    if (que) {
      Queue.default = que.queue;
    }
  }

  abstract process(job: Bull.Job, done: Bull.DoneCallback): void;
  onCompleted(job: Bull.Job) {
    const log = Logger(job.data.id);
    log.info(`Job completed`);
  }
  onCleaned = (jobs: Bull.Job[], type: Bull.JobStatusClean) => logger.info(`Cleaned ${jobs.length} ${type} jobs`);
  onQueueError = (error: Error) => Logger('Bull').error(error.message);
  onRedisError = (error: any) => Logger('Redis').error(error);

  private findByChannel = (channel: string) => queue => queue.channel === channel;
}

export class MessageQueue extends Queue {
  constructor(channel: string, options: QueueOptions) {
    super(channel, { ...options, concurrency: 3 });
  }

  process(job: Bull.Job, done: Bull.DoneCallback) {
    const log = Logger(job.data.id);
    log.info('Processing...');
    log.info(job.data);
    return done();
  }
}
