import { normalizePort } from './helpers';

const { REDIS_HOSTNAME, REDIS_PORT, REDIS_PASSWORD } = process.env;
const port = normalizePort(REDIS_PORT || 6379);

export const REDIS_CLIENT_OPTS = { port, host: REDIS_HOSTNAME || '127.0.0.1', password: REDIS_PASSWORD };
