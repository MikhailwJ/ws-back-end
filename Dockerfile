FROM node:lts-alpine

RUN apk update && apk upgrade
RUN npm i -g npm

ENV HOME=/home/app/

COPY . $HOME
RUN chown -R app:app $HOME
WORKDIR $HOME

RUN npm i --silent

USER app
RUN npm run build

CMD ["node", "dist/main.js"]

